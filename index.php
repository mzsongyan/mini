<?php

define('ROOT', __DIR__);

function autoLoad($path, $namespace = NULL)
{
    spl_autoload_register(function ($class) use ($path, $namespace) {
        if (!empty($namespace)) {
            if (0 == strpos(ltrim($class, '\\'), $namespace . '\\')) {
                $class = substr(ltrim($class, '\\'), strlen($namespace) + 1);
            } else {
                return;
            }
        }
        $file = $path . '/' . str_replace(array('_', '\\'), '/', $class) . '.php';
        if (file_exists($file)) {
            include_once $file;
        }
    });
}

autoLoad(ROOT . '/Vendor', 'Vendor');

autoLoad(ROOT . '/Modules', 'Modules');

$configs = [
    'app.name'    => 'syan',
    'root'        => __DIR__,
    'debug'       => true,
    'debug.log'   => false,
    'db.prefix' => 'test_',
    'db.host'   => '127.0.0.1',
    'db.dbname' => 'testdb',
    'db.user'   => 'root',
    'db.pw'     => ''
];

// 实例化应用
$app = new Vendor\Application($configs);

$app->registerErrorHandler();

$app->register(new Vendor\Provider\AclServiceProvider);

$app->register(new Vendor\Provider\CacheServiceProvider);

$app->register(new Vendor\Provider\DbServiceProvider);

$app->loadModules([ROOT . '/Modules']);

$app->trigger('app.init')->run();
