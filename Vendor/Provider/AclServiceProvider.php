<?php

namespace Vendor\Provider;

use Vendor\Interfaces\ServiceProviderInterface;
use Vendor\Application;
use Vendor\Acl;

class AclServiceProvider implements ServiceProviderInterface
{
	public function register(Application $app)
	{
		$app['acl'] = function() {
			return new Acl();
		};
	}

}